﻿using UnityEngine;
using System.Collections;

public class PlayerShip : Ship, IDynamicObject {
	// higher number bigger jump back on collision with wall
	int moveBackMultiplier = 2;
	// ship animations, left, right animations
	Animator animations;
	// exaust pipe list, for animations
	ExaustPipe[] exaustPipe;
	// referance to a GameManager object, exp: for spawning new player ship once this dies
	// maybe use SendMessage?
	GameManager manager;
	// refereance to AudioManager object
	AudioManager audioManager;
	// last time object was triggered
	float lastTrigger = 0f;
	// duration untill next time trigger can happen
	float nextTrigger = 3f;
	// exaust pipe placement around ship, maybe use gameObject within gameObject
	public float exaustCenterDistanceX = 0.1284285f;
	public float exaustCenterDistanceY = 0.5f;
	// exaust pipe prefab, for instatiation
	public ExaustPipe exaust;
	// ship projectile prefab, for instatiation/shooting
	public Projectile bullet;

	// Use this for initialization
	void Start () {
		base.SetUp ();
		// animator attached to object
		animations = this.GetComponent<Animator> ();
		// because of this nextTrigger also acts as invulnerability duration
		lastTrigger = Time.time;

		// instatiate exaust pipes of this ship
		exaustPipe = new ExaustPipe[2];
		exaustPipe[0] = (ExaustPipe) Instantiate(exaust, new Vector3(transform.position.x - exaustCenterDistanceX, transform.position.y + exaustCenterDistanceY, 0), Quaternion.identity);
		exaustPipe[1] = (ExaustPipe) Instantiate(exaust, new Vector3(transform.position.x + exaustCenterDistanceX, transform.position.y + exaustCenterDistanceY, 0), Quaternion.identity);

		// get GameManager and set current lives of ship to be displayed
		GameObject managerObject = GameObject.Find ("GameManager");
		manager = (GameManager)managerObject.GetComponent<GameManager> ();
		manager.SetPlayerLives(lives);

		GameObject audioObject = GameObject.Find ("AudioManager");
		audioManager = (AudioManager)audioObject.GetComponent<AudioManager> ();
		audioManager.PlayStart ();
	}
	
	// Update is called once per frame
	void Update () {
		if (!pause){
			Shoot ();
			Move ();
		}
	}

	public void Move(){
		// calculate values of movement in 2D, invert X axis movement
		float verticalMovement = Input.GetAxis ("Vertical") * speed * Time.deltaTime;
		float horizontalMovement = Input.GetAxis ("Horizontal") * speed * Time.deltaTime * -1;
		float currentPositionX = transform.position.x;
		float currentPositionY = transform.position.y;
		// can it move in horizontal range
		bool horzRangeBool = (horzExtent > currentPositionX) && (-horzExtent < currentPositionX);
		int horzRange = horzRangeBool.GetHashCode();
		// can it move in vertical range
		bool vertRangeBool = (vertExtent > currentPositionY) && (-vertExtent < currentPositionY);
		int vertRange = vertRangeBool.GetHashCode();
		// animate left and right movement
		AnimateMovement (horizontalMovement);
		// move in axis
		transform.Translate (new Vector3 (horizontalMovement * horzRange, verticalMovement * vertRange, 0));
		// move exaust pipes
		foreach(ExaustPipe pipe in exaustPipe){
			if (pipe != null) pipe.transform.Translate(new Vector3 (horizontalMovement * horzRange, verticalMovement * vertRange, 0));
			// correct exaust pipes so they stay under the ship
			if (pipe != null) pipe.transform.position = new Vector3 (pipe.transform.position.x, transform.position.y - 0.5f, 5f);
		}
		// invert boolean values to move back
		horzRange = (!horzRangeBool).GetHashCode();
		vertRange = (!vertRangeBool).GetHashCode();
		// move back
		MoveBack(currentPositionX * horzRange + currentPositionY * vertRange, horzRange, vertRange, horzExtent * horzRange + vertExtent * vertRange);
	}

	public void AnimateMovement(float horizontalMovement){
		// show animation either moving right or idle
		if (horizontalMovement < 0) {
			animations.SetBool ("right", true);
			exaustPipe[0].GetComponent<Animator>().SetBool("left", true);
			exaustPipe[1].GetComponent<Animator>().SetBool("left", true);
		}else{
			animations.SetBool("left", false);
			animations.SetBool ("right", false);
			exaustPipe[1].GetComponent<Animator>().SetBool("left", false);
			exaustPipe[1].GetComponent<Animator>().SetBool("right", false);
			exaustPipe[0].GetComponent<Animator>().SetBool("right", false);
			exaustPipe[0].GetComponent<Animator>().SetBool("left", false);
		}
		// show animation moving left
		if (horizontalMovement > 0) {
			animations.SetBool("left", true);
			exaustPipe[0].GetComponent<Animator>().SetBool("right", true);
			exaustPipe[1].GetComponent<Animator>().SetBool("right", true);
		}
	}

	/* currentPosition - the position of either X or Y 
	   useX - 1 to set X
	   useY - 1 to set Y
	   extent - size of axis to use
	   use with caution no protection from wrong number*/
	void MoveBack(float currentPosition, int useX, int useY, float extent){
		if (useX == useY) return;
		float sign = currentPosition/Mathf.Abs(currentPosition);
		float moveBack = currentPosition + (extent - Mathf.Abs(currentPosition)) * sign * moveBackMultiplier;
		float moveX = moveBack * useX + transform.position.x * useY;
		float moveY = moveBack * useY + transform.position.y * useX;
		transform.position = new Vector3(moveX, moveY, 5f);
		// move exaust pipes
		foreach(ExaustPipe pipe in exaustPipe){
			if (pipe != null) pipe.MoveBack (useX, useY, extent, moveBackMultiplier, this);
		}
	}

	void Shoot(){
		if (Input.GetKeyDown("space")){
			for (int i = numberOfBullets; i > 0; i--){
				Instantiate(bullet, new Vector3(transform.position.x + i/4, transform.position.y + 0.5f, 5f), Quaternion.identity);
			}
			audioManager.PlayShoot();
		}
	}

	// on trigger enter called more then once need to check time interval to protected from more triggers
	void OnTriggerEnter2D(Collider2D other) {
		if (other.gameObject.tag == "Hostile" && Time.time > lastTrigger + nextTrigger){
			Ship enemy = other.gameObject.GetComponent<Ship>();
			StartCoroutine(enemy.DestroyDelayed());
			lastTrigger = Time.time;
			StartCoroutine(DestroyDelayed());
		}
		if (other.gameObject.tag == "Harmful" && Time.time > lastTrigger + nextTrigger){
			lastTrigger = Time.time;
			StartCoroutine(DestroyDelayed());
		}
		if (other.gameObject.tag == "Projectile" && Time.time > lastTrigger + nextTrigger){
			Projectile enemy = other.gameObject.GetComponent<Projectile>();
			Destroy (enemy.gameObject);
			lastTrigger = Time.time;
			StartCoroutine(DestroyDelayed());
		}
	}

	public override IEnumerator DestroyDelayed(){
		lives--;
		Pause ();
		// destroy all exaust pipes as they are separate objects and wont be affected by explosion animation
		foreach(ExaustPipe pipe in exaustPipe){
			if (pipe != null) Destroy (pipe.gameObject);
		}

		//disabling movement animations, otherwise they may overwrite the explosion animation
		animations.SetBool("left", false);
		animations.SetBool("right", false);
		animations.SetBool("explode", true);
		// spawn new player ship if possible
		StartCoroutine (manager.SpawnPlayer());
		// wait until animations are done, replace seconds with animation length
		yield return new WaitForSeconds (3);
		Destroy (this.gameObject);
	}

	// increase life and display in GUI
	public override void IncreaseLife(int howMuch) {
		lives += howMuch;
		manager.SetPlayerLives (lives);
	}

	public override void IncreaseBullets(int howMuch) {
		numberOfBullets += howMuch;
	}

	public override void IncreaseSpeed(float howMuch) {
		speed += howMuch;
	}

}
