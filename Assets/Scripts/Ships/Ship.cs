﻿using UnityEngine;
using System.Collections;
// parent class for all ships
public class Ship : GalagaObject {
	// lives of a ship, enemy could have more than 1 too, exp: boss
	public int lives = 3;
	// how fast ships move
	public float speed = 6f;
	// how many bullets at one shot
	public int numberOfBullets = 1;

	// something hit ship, called from different object because of an issue with two colliders
	public virtual void Hit(int damage){}
	public virtual IEnumerator DestroyDelayed(){yield return null;}
	public virtual void IncreaseLife(int howMuch){}
	public virtual void IncreaseSpeed(float howMuch){}
	public virtual void IncreaseBullets(int howMuch){}
}
