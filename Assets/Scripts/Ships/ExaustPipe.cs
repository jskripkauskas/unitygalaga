﻿using UnityEngine;
using System.Collections;

public class ExaustPipe : MonoBehaviour {

	// move back same as ship, could probably be avoided with gameObject within gameObject
	public void MoveBack(int useX, int useY, float extent, int moveBackMultiplier, Ship ship){
		float currentPosition = transform.position.x;
		float sign = currentPosition/Mathf.Abs(currentPosition);
		float moveBack = currentPosition + (extent - Mathf.Abs(currentPosition)) * sign * moveBackMultiplier;
		float moveX = moveBack * useX + transform.position.x * useY;
		float moveY = ship.transform.position.y - 0.5f;
		transform.position = new Vector3(moveX, moveY, 0);
	}
}
