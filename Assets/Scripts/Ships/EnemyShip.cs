﻿using UnityEngine;
using System.Collections;

public class EnemyShip : Ship, IDynamicObject, ISelfDestructable {
	public float time;
	// bullet to shoot
	public Projectile bullet;
	// life item prefab, in case of many items variable should be changed to array
	public Item lifePrefab;
	// powerup item prefab
	public Item powerupPrefab;
	// how much score is it worth
	public int worth = 100;
	/* luck, higher number = lower luck on item spawning
	   50 luck = range 1 to 4x25, lucky numbers 50 and 25, 75 unlucky (enemy shoots)
	   more items = more shooting, hahahahahaha */
	public int luck = 25;
	// last change of direction to move
	float lastTurn = 0;
	// side for ship movement 1 - left, -1 -right
	float side = 1f;
	// set to true if ship has to fly away out of screen
	bool flyAway = false;
	// direction to fly away to
	float direction;
	// game manager - main controller of the game
	GameObject managerObject;
	GameManager manager;
	// needed to play animations
	Animator animations;
	// refereance to AudioManager object
	AudioManager audioManager;


	// Use this for initialization
	void Start () {
		base.SetUp ();
		animations = this.GetComponent<Animator> ();
		direction = Random.Range (-1, 2);
		managerObject = GameObject.Find ("GameManager");
		manager = (GameManager)managerObject.GetComponent<GameManager> ();

		GameObject audioObject = GameObject.Find ("AudioManager");
		audioManager = (AudioManager)audioObject.GetComponent<AudioManager> ();
	}
	
	// Update is called once per frame
	void Update () {
		if (!pause){
			if (!flyAway){
				Move ();
			} else{
				FlyAway();
			}
		}
	}

	// at some point fly out of screen and die, no reason for ship to fly in same pattern downwards
	void FlyAway(){
		transform.eulerAngles = new Vector3 (0, 0, 45f * direction);
		transform.Translate (new Vector3 (0, -10f * speed * Time.deltaTime, 0));

		// in case alien ship is below screen height + offset destroy it
		if (transform.position.y < -vertExtent - offset) {
			SelfDestruct();
		}
	}

	public void Move(){
		if (Time.time > lastTurn + time) {
			side = -side;
			lastTurn = Time.time;

			/*shoots every turn not at any kind of movement,
			  take out of this if in case shooting need to be rolled
			  every time object moves = taking it out would mean insane increase in shots
			  need separate timer*/
			int randomNumber = Random.Range (1, 4*luck);

			if (randomNumber == luck * 3){
			audioManager.PlayFly();
				
				for (int i = numberOfBullets; i > 0; i--){
					Instantiate(bullet, new Vector3(transform.position.x + i/4, transform.position.y - 0.25f, 5f), Quaternion.identity);
				}
			}
		}

		float horizontalMovement = 1f * speed * Time.deltaTime * side;
		transform.Translate (new Vector3 (horizontalMovement, -1f * speed * Time.deltaTime, 0));
		CheckLimit ();
	}

	// when certain point of screen is reached ship should start flying away
	void CheckLimit(){
		if (transform.position.y < -vertExtent/2 + 2f){
			flyAway = true;
		}
	}

	//invoked by other gameObject in order to tell this object that it has been hit
	public override void Hit(int damage){
		base.Hit (damage);
		lives = lives - damage;

		// in case ship has no more lives destroy it
		if (lives <= 0){
			StartCoroutine(DestroyDelayed());
		}
	}

	public override IEnumerator DestroyDelayed(){
		// show animation of explosion
		animations.SetBool("explode", true);
		audioManager.PlayKill ();
		manager.IncreaseScore (worth);
		Pause ();
		// turn off collider while animation is playing
		this.collider2D.enabled = false;
		// wait for animation to finish, TO DO: take time from animation
		yield return new WaitForSeconds (2);
		WheelOfFortune ();
		SelfDestruct ();
	}

	// generates random number in luck range and compares it to lucky numbers
	void WheelOfFortune(){
		int randomNumber = Random.Range (1, 4*luck);

		// else if = less checking in case of life item
		if (randomNumber == luck * 2){
			Instantiate(lifePrefab, transform.position, Quaternion.identity);
		} else{
			if (randomNumber == luck) {
				Instantiate(powerupPrefab, transform.position, Quaternion.identity);
			}
		}
	}

	// gets destroyed and reduces number of ships in game manager (needed so new ships could be spawn)
	public void SelfDestruct(){
		// reduce number of ships alive in a game
		manager.RemoveShip ();
		// destroy game object
		Destroy (this.gameObject);
	}

	public override void IncreaseSpeed(float howMuch) {
		speed += howMuch;
	}

}
