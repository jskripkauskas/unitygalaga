﻿using UnityEngine;
using System.Collections;

// responsible for background sprite and background object instantiation
// for different resolutions background as GUITexture may be better
public class Background : GalagaObject {
	// list of objects to spawn more often
	public NeutralObject[] commonObjects;
	// list of objects to spawn less often
	public NeutralObject[] uncommonObjects;
	// list of objects that will hurt you once they spawn
	public HarmfulObject[] harmfulObjects;
	// last time object was instantiated
	float lastCommon = 0f;
	// how often to spawn uncommon object
	float nextObject = 3f;
	// next object spawn time divided by a number for common object spawning
	float timerDivider = 10f;
	// last uncommon object
	float lastUncommon = 0f;
	// loop number, uncommon objects shouldn't spawn at the same time
	int loopCount = 0;
	// last harmful
	float lastHarmful = 0f;
	// also which harmful, since we dont want all at the same time
	int harmfulCount = 0;
	
	// Use this for initialization
	void Start () {
		base.SetUp ();
	}
	
	// Update is called once per frame
	void Update () {
		if (!pause){
			// spawn objects at given time, at random X axis location
			SpawnCommon();
			SpawnUncommon();
			SpawnHarmful ();
		}
	}

	// spawn common objects every given time
	void SpawnCommon (){
		if (Time.time > lastCommon + nextObject/timerDivider){
			foreach (NeutralObject neutral in commonObjects){
				float horizontal = Random.Range(-horzExtent, horzExtent);
				Instantiate(neutral, new Vector3(horizontal, vertExtent + 1f, 2f), Quaternion.identity);
			}

			lastCommon = Time.time;
		}
	}

	// spawn uncommon objects every given time, less often than common objects
	void SpawnUncommon (){
		if (Time.time > lastUncommon + nextObject*timerDivider){
			loopCount++;
			float horizontal = Random.Range(-horzExtent, horzExtent);

			if (loopCount >= uncommonObjects.Length){
				loopCount = 0;
			}
			Instantiate(uncommonObjects[loopCount], new Vector3(horizontal, vertExtent + 1f, 2f), Quaternion.identity);
			lastUncommon = Time.time;
		}
	}

	// spawn harmful objets every given time, less often than uncommon
	void SpawnHarmful (){
		if (Time.time > lastHarmful + nextObject*timerDivider*2){
			harmfulCount++;
			float horizontal = Random.Range(-horzExtent, horzExtent);
			
			if (harmfulCount >= harmfulObjects.Length){
				harmfulCount = 0;
			}
			Instantiate(harmfulObjects[harmfulCount], new Vector3(horizontal, vertExtent + 1f, 2f), Quaternion.identity);
			lastHarmful = Time.time;
		}
	}


}
