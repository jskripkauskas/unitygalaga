﻿using UnityEngine;
using System.Collections;

// upmost parent class, for inheritance since majority of ingame objects share some behaviour
public class GalagaObject : MonoBehaviour {
	// pause value, wether ingame object should be paused or not
	protected bool m_pause = false;
	// width of screen, Screen.Width != vertExtent
	protected float vertExtent;
	// height of screen, Screen.Height != horzExtent
	protected float horzExtent;
	// offset how far objects can fly off widht or height before they self destruct or move back
	public float offset = 5f;

	// pause variable getter/setter
	public bool pause
	{
		get{ return m_pause; }
		set{ m_pause = value; }
	}
	
	public virtual void Pause (){pause = true;}
	// better way pause = !pause, in this case would not work
	public virtual void Unpause (){pause = false;}

	// instantiate variables, not always needed though
	protected void SetUp () {
		vertExtent = Camera.main.orthographicSize * Screen.height / Screen.width + 1f;
		horzExtent = Camera.main.orthographicSize * Screen.width / Screen.height;
	}
}
