﻿using UnityEngine;
using System.Collections;

public class CameraController : MonoBehaviour {
	public Texture backgroundTexture;

	void OnGUI(){
		GUI.DrawTexture (new Rect (0, 0, Screen.width, Screen.height), backgroundTexture);

		if (GUI.Button(new Rect(Screen.width * 0.45f, Screen.height * 0.3f, Screen.width * 0.1f, Screen.height * 0.1f), "Start")){
			Application.LoadLevel("GalagaMain");
		}

		if (GUI.Button(new Rect(Screen.width * 0.45f, Screen.height * 0.5f, Screen.width * 0.1f, Screen.height * 0.1f), "Exit")){
			Application.Quit();
		}
	}
}
