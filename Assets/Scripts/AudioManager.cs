using UnityEngine;
using System.Collections;

public class AudioManager : MonoBehaviour {

	public AudioClip theme;
	public AudioClip shoot;
	public AudioClip kill;
	public AudioClip start;
	public AudioClip enemy;
	
	void Start()
	{
		PlayTheme ();
	}

	public void PlayTheme() 
	{
		// TO DO: readjust theme song
		//audio.Play();
	}
	
	public void PlayKill() 
	{
		audio.PlayOneShot(kill, 1f);
	}
	
	public void PlayShoot() 
	{	
		audio.PlayOneShot(shoot, 0.8f);
	}
	
	public void PlayStart() 
	{
		audio.PlayOneShot(start, 0.7f);
	}
	
	public void PlayFly() 
	{
		audio.PlayOneShot(enemy, 0.55f);
	}
}
