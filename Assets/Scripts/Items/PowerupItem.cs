﻿using UnityEngine;
using System.Collections;

public class PowerupItem : Item {
	// amount of lives to add
	public int lives = 1;
	// amount of speed increase
	public float speedInc = 0.1f;
	// amount of number of bullets increase
	public int bulletInc = 1;
	// limit beyond item wont increase life
	public int lifeLimit = 10;
	// speed limit beyond item wont increase speed
	public float speedLimit = 4f;
	// bullet limit beyond item wont increase number of bullets
	// should not exceed 3, TO DO: make it more dynamic to go beyond 3
	public int bulletLimit = 3;
	int numberOfEffects = 3;
	int generatedEffect;

	// Use this for initialization
	void Start () {
		generatedEffect = Random.Range (0, numberOfEffects);
	}
	
	// Update is called once per frame
	void Update () {
		if (!pause){
			// if does not exist in this class tooks up in parent class
			Move ();
		}
	}

	// on trigger enter called more then once need to check time interval to protected from more triggers
	void OnTriggerEnter2D(Collider2D other) {
		if (other.gameObject.tag == "Player" && Time.time > lastTrigger + nextTrigger){
			Ship player = other.gameObject.GetComponent<Ship>();

			switch (generatedEffect)
			{
			case 0: 
				if (player.lives < lifeLimit) player.IncreaseLife(lives);
				break;
			case 1:
				if (player.speed < speedLimit) player.IncreaseSpeed(speedInc);
				break;
			case 2:
				if (player.numberOfBullets < bulletLimit) player.IncreaseBullets(bulletInc);
				break;
			default:
				if (player.lives < lifeLimit) player.IncreaseLife(lives);
				break;
			}

			lastTrigger = Time.time;
			Destroy (this.gameObject);
		}
	}
}
