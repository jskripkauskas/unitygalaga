﻿using UnityEngine;
using System.Collections;

public class Item : GalagaObject, IDynamicObject, ISelfDestructable {
	public float speed = 5f;
	// last time object was triggered
	protected float lastTrigger = -3f;
	// also provides sorts of invulnerability on spawn
	protected float nextTrigger = 3f;	

	// implemented in parent class to provide with default behaviour
	public virtual void Move(){
		transform.Translate (new Vector3 (0, -1f * speed * Time.deltaTime, 0));

		if (transform.position.y < -vertExtent - offset) {
			SelfDestruct();
		}
	}

	// implemented in parent class to provide with default behaviour
	public virtual void SelfDestruct(){
		// destroy game object
		Destroy (this.gameObject);
	}
}
