﻿using UnityEngine;
using System.Collections;

public class LifeItem : Item {
	public int lives = 1;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if (!pause){
			// if does not exist in this class tooks up in parent class
			Move ();
		}
	}

	// on trigger enter called more then once need to check time interval to protected from more triggers
	void OnTriggerEnter2D(Collider2D other) {
		if (other.gameObject.tag == "Player" && Time.time > lastTrigger + nextTrigger){
			Ship player = other.gameObject.GetComponent<Ship>();
			player.IncreaseLife(lives);
			lastTrigger = Time.time;
			Destroy (this.gameObject);
		}
	}

}
