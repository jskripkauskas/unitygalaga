﻿using UnityEngine;
using System.Collections;

// manages whole game, could be attached to camera instead of separate object
public class GameManager : GalagaObject {
	// player ship prefab, used to load player ship into game
	public Ship mainShip;
	// alien ship prefab, used for enemies, in case of many different enemy ships use array
	public Ship aliens;
	// default height of alien ship pattern
	public float defaultHeight = 1f;
	// GUI texts for displaying text or numbers, game info, etc.
	public GUIText livesNumber;
	public GUIText scoreNumber;
	public GUIText gameOverText;
	public GUIText finalScoreText;
	public GUIText finalScoreNumber;
	public GUIText levelNumber;
	// higher number - easier, lower - harder
	public int difficulty = 50;
	// number of ships still in game, used in order to spawn new wave and increase level
	int numberOfShipsAlive;
	// variables responsible for keeping data about lives and score, mainly to be displayed later
	int livesDisplayed = 3;
	int scoreDisplayed = 0;
	// variable for displaying level and also difficulty increase
	int level = 0;

	// Use this for initialization
	void Start () {
		Instantiate(mainShip, new Vector3(0, -4f, 5f), Quaternion.identity);
		SpawnEnemy ();
	}
	
	// Update is called once per frame
	void Update () {
		// Time.timeScale instead, better?
		if (!pause){
			// once 0 reached spawn new wave of enemies, TO DO: 0 could be put in public variable and adjusted
			if (numberOfShipsAlive <= 0) {
				SpawnEnemy();
				IncreaseSpeed ();
			}

			// display info in GUI text
			livesNumber.text = livesDisplayed.ToString ();
			scoreNumber.text = scoreDisplayed.ToString ();
			levelNumber.text = level.ToString ();
		}

		// in case "P" is pressed pause most of the game, or unpause
		if (Input.GetKeyDown("p")){
			GalagaObject[] objectArray = (GalagaObject[]) GameObject.FindObjectsOfType(typeof(GalagaObject));

			if (!pause){
				PauseAll(objectArray);
			}else{
				UnpauseAll(objectArray);
			}
		}

		// in case of "Esc" being pressed, quit this level and go back to menu
		if (Input.GetKeyDown(KeyCode.Escape)){
			Application.LoadLevel(Application.loadedLevel - 1);
		}
	}

	// increases speed of alien ships by level = difficulty increase
	void IncreaseSpeed(){
		GameObject[] objectArray = GameObject.FindGameObjectsWithTag("Hostile");

		foreach (GameObject shipObject in objectArray){
			Ship ship = (Ship) shipObject.GetComponent<Ship> ();
			ship.IncreaseSpeed(level/50);
		}
	}

	// Spawn random number of alien ships in random pattern
	void SpawnEnemy(){
		int numberOfLines = Random.Range (2,7);
		float curve = Random.Range (1f, 2f);
		int method = Random.Range (0, 4);
		level++;

		for (int line = numberOfLines; line > 0; line--){
			int numberOfShip = Random.Range(1,13);
			numberOfShipsAlive += numberOfShip * 2;

			for (int i = -numberOfShip; i < numberOfShip; i++){
				Instantiate(aliens, new Vector3(i/2f, GenerateYCurve(i, line, curve, method), 5f), Quaternion.identity);
			}
		}
	}

	// Generates pattern that newly spawn ships will be placed in
	float GenerateYCurve(int i, int line, float curve, int method){
		float result = 0;

		switch (method)
		{
		case 0: 
			// line
			result = 0.5f + line/2f + defaultHeight;
			break;
		case 1:
			// sin curve
			result = Mathf.Sin(i/curve) * 0.5f + line/2f + defaultHeight;
			break;
		case 2:
			// cos curve
			result = Mathf.Cos(i/curve) * 0.5f + line/2f + defaultHeight;
			break;
		case 3:
			// arctan curve
			result = Mathf.Atan(i/curve) * 0.5f + line/2f + defaultHeight;
			break;
		default:
			// line
			result = 0.5f + line/2f + defaultHeight;
			break;
		}

		return result;
	}

	// remove one ship, not to call directly, is it a thing in C#?
	public void RemoveShip(){
		numberOfShipsAlive--;
	}

	public void SetPlayerLives(int lives){
		this.livesDisplayed = lives;
	}

	public void IncreaseScore(int increment){
		this.scoreDisplayed += increment;
	}

	// spawn player ship according to lives left or show game over
	public IEnumerator SpawnPlayer(){
		livesDisplayed--;

		if (livesDisplayed > 0){
			yield return new WaitForSeconds (3);
			Instantiate(mainShip, new Vector3(0, -4f, 5f), Quaternion.identity);	
			GameObject[] playerObject = GameObject.FindGameObjectsWithTag("Player");
			Ship playerShip = (Ship)playerObject[playerObject.Length - 1].GetComponent<Ship> ();
			playerShip.lives = livesDisplayed;
		}else{
			GameOver ();
		}
	}

	// pauses the game and provides player with the info that he lost - you cant win this though
	void GameOver(){
		gameOverText.enabled = true;
		finalScoreText.enabled = true;
		finalScoreNumber.enabled = true;
		finalScoreNumber.text = scoreDisplayed.ToString();
		
		GalagaObject[] objectArray = (GalagaObject[]) GameObject.FindObjectsOfType(typeof(GalagaObject));
		PauseAll (objectArray);
	}

	// pause most of ingame objects, non-ingame objects are permited work
	void PauseAll(GalagaObject[] objectArray){
		foreach (GalagaObject galagaObject in objectArray){
			galagaObject.Pause();
		}
	}

	// unpause most of ingame objects
	void UnpauseAll(GalagaObject[] objectArray){
		foreach (GalagaObject galagaObject in objectArray){
			galagaObject.Unpause();
		}
	}

}
