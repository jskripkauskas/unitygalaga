﻿using UnityEngine;
using System.Collections;

public interface ISelfDestructable {
	void SelfDestruct();
}
