﻿using UnityEngine;
using System.Collections;

public class Projectile : GalagaObject, ISelfDestructable {

	public int damage = 1;
	public int speed = 7;

	public void SelfDestruct(){
		// destroy game object
		Destroy (this.gameObject);
	}
}
