﻿using UnityEngine;
using System.Collections;

public class Bullet : Projectile, IDynamicObject {
	// last time object was triggered
	float lastTrigger = -3f;
	// wont trigger so fast again
	float nextTrigger = 3f;

	// Use this for initialization
	void Start () {
		base.SetUp ();
	}
	
	// Update is called once per frame
	void Update () {
		if (!pause){
			Move ();
		}
	}

	public void Move(){
		float verticalMovement = speed * Time.deltaTime;
		transform.Translate (new Vector3 (0, verticalMovement, 0));

		if (vertExtent < transform.position.y){
			SelfDestruct();
		}
	}

	// on trigger enter called more then once need to check time interval to protected from more triggers
	// TO DO: look it up, there should be better fix
	void OnTriggerEnter2D(Collider2D col) {
		if (col.gameObject.tag == "Hostile" && Time.time > lastTrigger + nextTrigger){
			lastTrigger = Time.time;
			Ship shot = col.gameObject.GetComponent<Ship>();
			shot.Hit(damage);
			Destroy(this.gameObject);
		}
		if (col.gameObject.tag == "Harmful" && Time.time > lastTrigger + nextTrigger){
			lastTrigger = Time.time;
			Destroy(this.gameObject);
		}
	}

}
