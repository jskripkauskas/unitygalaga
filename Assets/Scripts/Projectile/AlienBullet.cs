﻿using UnityEngine;
using System.Collections;

public class AlienBullet : Projectile, IDynamicObject {
	// Use this for initialization
	void Start () {
		base.SetUp ();
	}
	
	// Update is called once per frame
	void Update () {
		if (!pause){
			Move ();
		}
	}
	
	public void Move(){
		float verticalMovement = -1f * speed * Time.deltaTime;
		transform.Translate (new Vector3 (0, verticalMovement, 0));
		
		if (transform.position.y < -vertExtent - offset) {
			SelfDestruct();
		}
	}

}
