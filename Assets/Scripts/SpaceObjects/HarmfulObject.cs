﻿using UnityEngine;
using System.Collections;

public class HarmfulObject : GalagaObject, IDynamicObject, ISelfDestructable {
	public float speed = 4f;

	// Use this for initialization
	void Start () {
		base.SetUp ();
	}
	
	// Update is called once per frame
	void Update () {
		if (!pause){
			Move ();
		}
	}

	public void Move(){
		transform.Rotate (new Vector3(0, 0, 0.1f));
		transform.rigidbody2D.MoveRotation (0.1f);;
		transform.position += Vector3.down * speed * Time.deltaTime;
	
		if (transform.position.y < -vertExtent - offset) {
			SelfDestruct();
		}
	}
	
	public void SelfDestruct(){
		// destroy game object
		Destroy (this.gameObject);
	}
}
