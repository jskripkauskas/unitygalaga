﻿using UnityEngine;
using System.Collections;

public class NeutralObject : GalagaObject, IDynamicObject, ISelfDestructable {
	public float speed = 4f;

	// Use this for initialization
	void Start () {
		base.SetUp ();
	}
	
	// Update is called once per frame
	void Update () {
		if (!pause){
			Move ();
		}
	}

	public void Move(){
		float verticalMovement = -1f * speed * Time.deltaTime;
		transform.Translate (new Vector3 (0, verticalMovement, 0));
		
		if (transform.position.y < -vertExtent - offset) {
			SelfDestruct();
		}
	}

	public void SelfDestruct(){
		// destroy game object
		Destroy (this.gameObject);
	}
}
